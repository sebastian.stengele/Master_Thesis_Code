# Master Thesis Code

This script simulates the [gravitational quantum switch][1] on a lattice. 
To run the code install the requirements
```
pip install -r requirements.txt
```
and execute it:
```
python main.py
```
The parameters can be adjusted in the code itself.


[1]: https://doi.org/10.1038/s41467-019-11579-x

