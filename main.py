

import math
import time
from functools import lru_cache
import numpy as np
import matplotlib.pyplot as plt
import scipy
import scipy.linalg

## Parameters:

# Schwarzschild radius in lattice sites
rs = 22

# Distances mass-clock
r1 = 1.5*rs
r2 = 25*rs
clock_dists = [r1, r2]

# Distance particle-clock (in lattice sites)
dist_particle_clock = 50

# Interaction strength
alpha = 15
# Time scale
beta = 4.6/(dist_particle_clock)


## lattice sizes/setup:
# Particle lattice:
# 2 sigma_p | p0 | dist_particle_clock | c0 | 2 sigma_c | mirror | 2 sigma_c | dist_particle_clock | 2 sigma_p
# Clock lattice: (note: this is shifted such that c0 is on the same position in both lattices)
#                            2 sigma_c | c0 | 2sigma_c  

# spread of the particle
sigma_p = 5
# spread of the clock
sigma_c = 5

# particle lattice size
plat_dim = int(2*dist_particle_clock + 4*sigma_p + 4*sigma_c)
# particle initial position
plat_pos = 2*sigma_p
# internal dimension
pint_dim = 2

# clock lattice size
clat_dim = int(4*sigma_c)
# clock position
clat_pos = clat_dim/2
# internal dimension
cint_dim = 4

# shift between particle lattice and clock lattice
c_pos_p = int(2*sigma_p + dist_particle_clock - 2*sigma_c)
# number of evolution steps
ev_steps = int(2*dist_particle_clock + 4*sigma_c)


## Calculating the time-dilation for each point

# constants
g_dim = 2
g_scale = np.ones((g_dim, clat_dim))

def g_term(r):
    """Return the time dilation at position r (in the Schwarzschild metric)."""
    return np.sqrt((1-rs/r))

def g_lat_term(pos, c_dist):
    """Return a list of time dilations.
    
    pos -- a list of positions for wich to calculat the time dilation.
    c_dist -- the distance between the clock and the mass.
    """
    r = c_dist + clat_pos - np.array(pos)
    return g_term(r)

# Calculate the list of time dilation factors
g_list = np.array([g_lat_term(range(clat_dim), r1), g_lat_term(range(clat_dim), r2)])

## A collection of helper functions
def gaussian(dim, mu, sigma=2, v=False):
    """Return an array containing a Gaussian state.
    
    dim -- length of the array to return 
    mu -- position of the maximum
    sigma -- unnormalized standard deviation, std = sigma*dim/2
    v -- additionally return range(dim) 
    """
    r = np.array(range(dim))
    res = np.array([math.exp(-(x-mu)**2/(4*sigma**2))/(2*math.pi*sigma**2)**(1/4) for x in r])
    if v:
        return r, res
    return res

def ptrace(lat, system='clock'):
    """Calculate the partial trace over the clock or the system."""
    if system == 'particle':
        return np.einsum('aijkl,bmjol->abimko', lat, lat.conj())
    elif system == 'clock':
        return np.einsum('aijkl,binkp->abjnlp', lat, lat.conj())
    raise ValueError('system: {} not defined'.format(system))

def plotlattice(lat, c=0, cmap='Blues', vmin=0, vmax=1):
    """Plot a heatmap of the lattice."""
    plt.figure()
    plt.imshow(np.abs(lat[:,:,c]*2/100), cmap=cmap, interpolation='nearest', vmin=vmin, vmax=vmax)
    plt.show()
    
def plotparticle(lat, c=0, cmap='Blues', vmin=0, vmax=0.1, retim=False):
    """Plot a heatmap of the particle."""
    if not retim:
        plt.figure()
    im = plt.imshow(np.real(ptrace(lat)[0,0,:,:,c,c]), cmap=cmap, interpolation='nearest', vmin=vmin, vmax=vmax)
    if retim:
        return im
    plt.show()

def plotclock(lat, c=0, cmap='Blues', vmin=0, vmax=0.1, retim=False):
    """Plot a heatmap of the clock."""
    if not retim:
        plt.figure()
    im = plt.imshow(np.real(ptrace(lat, 'particle')[0,0,:,:,c,c]), cmap=cmap, interpolation='nearest', vmin=vmin, vmax=vmax)
    if retim:
        return im
    plt.show()   
    
def H(rho):
    """Calculate the von Neumann entropy of a density operator."""
    return - np.trace(np.dot(rho, scipy.linalg.logm(rho)))/np.log(2)

def H_lat(lat):
    """Calculate the von Neumann entropy of the lattice."""
    rho = np.einsum('i,j -> ij', lat.conj(), lat)
    return H(rho)

def H_cond(rho):
    """Calculate the conditional entropy of a density operator."""
    rhoB = np.einsum('jkii->jk', rho)
    return H(np.einsum('ijkl->kilj',rho).reshape((4,4))) - H(rhoB)

def Matrix_form(rho):
    """Return an approximate matrix of a density operator"""
    return np.around(np.einsum('ijkl->kilj',rho).reshape((4,4)),1)

## Operators
# Haddamards
Haddamard = np.array([[1,1],[-1,1]], dtype=complex)/math.sqrt(2)
Haddamard4 = np.einsum('ji,kl->jikl', Haddamard, Haddamard)
Haddamard4T = np.transpose(Haddamard4,axes=(3,2,1,0))
# Projectors onto the fully entangled state
fully_ent_vect = np.array([[0,1],[1,0]], dtype=complex)/math.sqrt(2)
fully_ent_state = np.einsum('ij,kl->ikjl', fully_ent_vect, fully_ent_vect)
# Bit flip on the particle
Xp = np.array([[0,1], [1,0]])
# Identities
Idp = np.identity(pint_dim)
Idc = np.identity(cint_dim)
# Bit flip of the particle on the full system
X = np.einsum('ij,kl->ikjl', Idc, Xp)

# Permutation matrix
P_mat_tmpc = np.array([[0,1,0,0],[0,0,1,0],[0,0,0,1],[1,0,0,0]]).T
# Clock hamiltonian
Hcc = 1j*scipy.linalg.logm(P_mat_tmpc)
# Clock hamiltonian on the full system
Hc = np.einsum('ij,kl->ikjl', Hcc, Idp).reshape(cint_dim*pint_dim, cint_dim*pint_dim)

# Interaction hamiltonian (modified matrixlog of a CNOT )
Hint = np.einsum('ij,kl->ikjl', Idc-np.diag([1,1,-1,-1]), Idp-Xp).reshape(cint_dim*pint_dim, cint_dim*pint_dim)

# Local evolution matrices

@lru_cache(maxsize=4)
def U_tot(t, alpha=alpha):
    """Return the full time evolution matrix."""
    return scipy.linalg.expm(-1j*t*beta*(Hc + alpha*Hint)).reshape(cint_dim, pint_dim, cint_dim, pint_dim)

@lru_cache(maxsize=4)
def U_c(t):
    """Return the clock time evolution matrix."""
    return scipy.linalg.expm(-1j*t*beta*Hc).reshape(cint_dim, pint_dim, cint_dim, pint_dim)

# global evolution
def Int_Evolution(lat, t=1, g=g_list, alpha=alpha, epsilon=1e-8):
    """Return the time evolution of the lattice
    
    lat -- lattice to evolve
    t -- time of the evolution
    g -- time dilation factors
    alpha -- interaction strength
    epsilon -- threshold to neglect matrix elements
    """
    res = np.empty_like(lat)
    if g is None:
        # Create a dummy list
        # g acts only on the clock, as there is no internal H of the particle
        g = np.ones((g_dim, clat_dim))
    ## Sum over the lattice
    # g-axis (different clock distances)
    for gi, glat in enumerate(lat):
        # Position of the clock
        for cp, clat in enumerate(glat):
            # Position of the particle
            for pp, plat in enumerate(clat):
                # Local, internal evolution
                
                # Neglect entries below threshold 
                if np.sum(abs(plat)) < epsilon:
                    res[gi, cp, pp] = 0
                # Full evolution if the particle and clock positions coincide
                if cp == pp - c_pos_p:
                    res[gi, cp, pp] = np.einsum('ijkl, kl->ij', U_tot(t*g[gi, cp], alpha=alpha), plat)
                # Evolve only the clock otherwise
                else:
                    res[gi, cp, pp] = np.einsum('ijkl, kl->ij', U_c(t*g[gi, cp]), plat)
    return res

def proj_part_pos(lat, pp, sigma=sigma_p):
    """Project the particle onto a gaussian state."""
    pstate = gaussian(plat_dim, pp, sigma)
    res = np.einsum('s, ijslm, njtlr, t-> inmr', pstate.conj(), lat, lat.conj(), pstate)
    proba = np.einsum('iijk->jk', res)
    return res, proba

def main(alpha=alpha):
    
    ## Build the initial state of the lattice
    
    clock_lattice = gaussian(clat_dim, clat_pos, sigma_c)
    clock_int = np.zeros(cint_dim, dtype=complex)
    clock_int[0] = 1
    part_lattice = gaussian(plat_dim, plat_pos, sigma_p)
    part_int = np.zeros(pint_dim, dtype=complex)
    part_int[0] = 1
    g_int = np.ones(g_dim, dtype=complex)/math.sqrt(2)
    # create the lattice
    lattice = np.einsum('i,j,k,l,m->ijklm', g_int, clock_lattice, part_lattice, clock_int, part_int)
    
    ## Print simulation info
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    print('Simulating: ')
    print('Alpha:         ', alpha)
    print('lattice shape: ', lattice.shape)
    print('lattice size:  ', lattice.size)
    print('# steps:       ', ev_steps)
    
    ## Calculate the time evolution
    # Prepare lists for the internal states
    clock_rho = []
    part_rho = []
    
    # Calculate the evolution steps
    for i in range(ev_steps):
        # Display progress
        if int(i*1000/ev_steps)%10 == 0:
            print('|', end='')
        # Internal evolution
        lattice = Int_Evolution(lattice, alpha=alpha)
        # Shift the lattice to simulate the movement of the particle
        lattice = np.roll(lattice, 1,axis=2)
        # Extract the internal states by tracing out
        part_rho.append(np.einsum('aijkl,bijkp->ablp', lattice, lattice.conj()))
        clock_rho.append(np.einsum('aijkl,bijol->abko', lattice, lattice.conj()))
    print('|')
    
    ## Plot clock probability
    
    # Calculate clock probabilities
    crho_diag = np.einsum('iaajj->aij', np.array(clock_rho))*2
    
    fig, axes = plt.subplots(2,1, sharex=True)
    # Plot probabilities
    for i in range(cint_dim):
        axes[0].plot(crho_diag[0][:,i], label='$|{}\\rangle_C$'.format(i))
    for i in range(cint_dim):
        axes[1].plot(crho_diag[1][:,i], label='$|{}\\rangle_C$'.format(i))
    
    # Set labels and layout
    axes[0].set_ylabel('probability')
    axes[0].set_title('Near ($d = {:.1f} r_S$)'.format(r1/rs), fontsize=12)
    axes[1].set_xlabel('evolution time(a.u.)')
    axes[1].set_ylabel('probability')
    axes[1].set_title('Far ($d = {:.1f} r_S$)'.format(r2/rs), fontsize=12)
    st = fig.suptitle('Clock state probabilities vs evolution time', fontsize=14)
    fig.tight_layout()
    st.set_y(0.96)
    fig.subplots_adjust(top=0.83, right=0.85)
    handles, labels = axes[1].get_legend_handles_labels()
    fig.legend(handles, labels, loc='center right')
    plt.savefig('clock_proba.png')
    plt.show()
    
    ## Plot particle probabilities
    
    # Calculate particle state in {+,-} basis 
    # Calculate projective measurement of fully entangled state
    proj_part = []
    hadm_part = []
    for p in part_rho:
        tmp_proj = np.einsum('ijkl,miln->mjkn',fully_ent_state, p)
        proj_part.append(np.einsum('iijj->', tmp_proj))
        tmp_had = np.einsum('ijkl,jmln->imkn',Haddamard4, p)
        hadm_part.append(np.einsum('ijkl,jmln->imkn',tmp_had,Haddamard4T))
    
    # Plot measurement probability of fully entangled state
    plt.figure()
    plt.plot(proj_part, label='')
    plt.xlabel('evolution time(a.u.)')
    plt.ylabel('probability')
    plt.ylim(0,1)
    plt.title('Probability of measuring $\Psi_{00}$ vs evolution time', fontsize=14)
    plt.savefig('particle_meas_proba.png')
    plt.show()
    
    # Calculate conditional entropies
    h_list = []
    for p in part_rho:
        h_list.append(H_cond(p))
        
    # Plot conditional entropies
    plt.figure()
    plt.plot(h_list, label='')
    plt.xlabel('evolution time(a.u.)')
    plt.ylabel('probability')
    
    plt.title('Conditional entropy vs evolution time', fontsize=14)
    plt.savefig('particle_cond_entropy.png')
    plt.show()
    
    # Calculate particle probabilities in {0,1} and {+,-} basis
    
    prho_diag = np.einsum('iaajj->aij', np.array(part_rho))*2
    prho_hadm_diag = np.einsum('iaajj->aij', np.array(hadm_part))*2
    
    # Plot particle probabilities in {0,1} basis
    plt.figure()
    for i in range(pint_dim):
        plt.plot(prho_diag[0][:,i], label='near ($d = {0:.1f} r_S$) $\\rho_{{{1}{1}}}$'.format(r1/rs,i))
    for i in range(pint_dim):
        plt.plot(prho_diag[1][:,i], label='far ($d = {0:.1f} r_S$) $\\rho_{{{1}{1}}}$'.format(r2/rs,i))
    plt.xlabel('evolution time(a.u.)')
    plt.ylabel('probability')
    plt.legend()
    plt.title('Particle state probabilities vs evolution time', fontsize=14)
    plt.savefig('particle_proba.png')
    plt.show()
    
    # Plot particle probabilities in {+,-} basis
    fig, ax = plt.subplots()
    state_label = {0:'{++}',1:'{--}'}
    # Create inset
    axins = ax.inset_axes([0.24, 0.65, 0.40, 0.3])
    # sub region of the original image
    x1, x2, y1, y2 = 55, 65, 0.42, 0.5
    axins.set_xlim(x1, x2)  
    axins.set_ylim(y1, y2)
    axins.set_xticklabels('')
    axins.set_yticklabels('')
    # Plot
    for i in range(pint_dim):
        ax.plot(prho_hadm_diag[0][:,i], label='near + far, $\\rho_{}$'.format(state_label[i]))
        axins.plot(prho_hadm_diag[0][:,i])
    for i in range(pint_dim):
        ax.plot(prho_hadm_diag[1][:,i], label='near - far, $\\rho_{}$'.format(state_label[i]))
        axins.plot(prho_hadm_diag[1][:,i])
    plt.xlabel('evolution time(a.u.)')
    plt.ylabel('probability')
    plt.legend()
    plt.title('Particle state probabilities (x-basis) vs evolution time', fontsize=14)
    
    ax.indicate_inset_zoom(axins)
    
    plt.savefig('particle_x_proba.png')
    plt.show()
    
    # Calculate probabilities in mixed state
    prho_mixed = np.einsum('iaajk->ijk', np.array(part_rho))
    prho_mixed_x = []
    for p in prho_mixed:
        tmp_had = np.dot(Haddamard, p)
        prho_mixed_x.append(np.dot(tmp_had,Haddamard.T))
    prho_mixed_x = np.array(prho_mixed_x)
    
    # Plot mixed states
    plt.figure()
    for i in range(pint_dim):
        plt.plot(prho_mixed[:,i,i], label='mixed $\\rho_{{{0}{0}}}$'.format(i))
    for i in range(pint_dim):
        plt.plot(prho_mixed_x[:,i,i], label='mixed $\\rho_{}$'.format(state_label[i]))
    plt.title('Particle state probabilities, gravity traced out', fontsize=14)
    plt.legend()
    plt.xlabel('evolution time (a.u.)')
    plt.ylabel('probability')
    plt.savefig('particle_proba_mixed.png')
    plt.show()
    
    # Calculate and print measurement probabilities
    mment, proba = proj_part_pos(lattice, plat_pos+ev_steps)
    mment_diag = np.einsum('iijj -> ij', mment)
    print('r1 proba: ', np.around(np.real(proba[0,0]), 4))
    print('r1 mment: ', np.around(np.real(mment_diag[0])/np.real(proba[0,0]), 4))
    print('r1 overlaps: ', np.around(np.real(prho_diag[0,-1]), 4))
    
    print('r2 proba: ', np.around(np.real(proba[1,1]), 4))
    print('r2 mment: ', np.around(np.real(mment_diag[1])/np.real(proba[1,1]), 4))
    print('r2 overlaps: ', np.around(np.real(prho_diag[1,-1]), 4))
    
    return lattice, clock_rho, part_rho
    
if __name__ == "__main__":
    alpha_list = [15,]
    t = [time.monotonic(), ]
    for al in alpha_list:
        lattice, crho, prho = main(alpha=al)
        t.append(time.monotonic())
    
    # Plot runtime if more than one alpha is used.
    if len(alpha_list) > 1:
        f, ax = plt.subplots(1)
        plt.plot(alpha_list, np.subtract(t[1:], t[:-1]), 'bo')
        ax.set_ylim(bottom=0, top=max(np.subtract(t[1:], t[:-1]))*1.1)
        plt.show(f)
    
    
    
    
    
    
    
    
    
    
    